#!/usr/bin/env python

# This file is part of FS22_Vehicle_Years.
#
# FS22_Vehicle_Years is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# FS22_Vehicle_Years is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# FS22_Vehicle_Years. If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path
from typing import Final
from zipfile import ZipFile
from argparse import ArgumentParser

OUTPUT_FILE_NAME: Final[str] = "FS22_Vehicle_Years.zip"


def pack_mod(output_dir: Path):
    """
    Packs the mod into a single ZIP file that contains all the necessary files

    Keyword arguments:
    output_dir (Path) -- The path of the output directory (if supplied). The
                         ZIP file will be placed in this directory with the
                         name "FS22_vehicle_years.zip"

    """
    output_file: Final[str] = Path.joinpath(output_dir, OUTPUT_FILE_NAME)

    with ZipFile(output_file.absolute(), "w") as z:
        z.write("vehicle_years.lua")
        z.write("modDesc.xml")
        z.write("icon_VY.dds")
        z.write("guiProfiles.xml")
        z.write(str(Path("data").joinpath("vehicle_years.xml")))
        z.write(str(Path("data").joinpath("vehicle_years_mods.xml")))

    print("Mod saved as " + str(output_file.absolute()))


if __name__ == "__main__":
    parser = ArgumentParser(description="Arguments for the packer")
    parser.add_argument(
        "--output_dir",
        type=str,
        required=True,
        help="The directory to output the zipped mod",
    )

    args = parser.parse_args()

    output_path = Path(args.output_dir)
    pack_mod(output_path)
